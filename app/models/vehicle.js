// declaring required
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// creating model
var VehicleSchema = new Schema({
  make: String,
  model: String,
  color: String
});

//exporting the model so it can be used else where
module.exports = mongoose.model('Vehicle', VehicleSchema);
